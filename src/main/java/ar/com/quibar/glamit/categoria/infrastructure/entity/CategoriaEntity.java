package ar.com.quibar.glamit.categoria.infrastructure.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import lombok.Builder;

@Document(value = "categoria",collection = "categoria")
@CompoundIndex(name = "nombre", unique = true)
public class CategoriaEntity extends Categoria {

	@Builder
	public CategoriaEntity(String id, String nombre, String codigo) {
		super(id, nombre, codigo);
	}

	public CategoriaEntity(Categoria categoria) {
		super(categoria.getId(),categoria.getNombre(), categoria.getCodigo());
	}
	
	public CategoriaEntity() {
	}

	@Id
	@Override
	public String getId() {
		return super.getId();
	}
}
