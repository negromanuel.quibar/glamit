package ar.com.quibar.glamit.categoria.domain.port;

import java.util.List;
import java.util.Optional;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;

public interface CategoriaRepository {

	Optional<Categoria> save(Categoria categoria);
	
	Optional<Categoria> findOneByNombre(String nombre);

	void deleteAll();
	
	List<Categoria> findAll();

	List<Categoria> saveAll(List<Categoria> categorias);
}
