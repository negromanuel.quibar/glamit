package ar.com.quibar.glamit.producto.infrastructure.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;

import ar.com.quibar.glamit.producto.infrastructure.api.request.FindProductoRequest;
import ar.com.quibar.glamit.producto.infrastructure.exceptions.ProductoHTTPInvalidException;
import ar.com.quibar.glamit.shared.infrastructure.validation.AbstractValidator;

@Component
public class FindProductoValidator extends AbstractValidator<FindProductoRequest> {

	
	public FindProductoValidator(final Validator validator) {
		super(validator);
	}

	@Override
	public Class<FindProductoRequest> getValidationClass() {
		return FindProductoRequest.class;
	}
	
	@Override
	public void validate(Object o) {
		super.validate(o);
		
		final var object = (FindProductoRequest) o;
		try {
			object.getPage();
			object.getSize();
		} catch (NumberFormatException e) {
			throw new ProductoHTTPInvalidException(e.getMessage());
		}
		
	}
	
}
