package ar.com.quibar.glamit.producto.infrastructure.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;

import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.shared.infrastructure.validation.AbstractValidator;

@Component
public class ProductoValidator extends AbstractValidator<Producto>{

	public ProductoValidator(final Validator validator) {
		super(validator);
	}
	@Override
	public Class<Producto> getValidationClass() {
		return Producto.class;
	}
	

}
