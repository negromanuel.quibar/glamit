package ar.com.quibar.glamit.shared.infrastructure.security;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {
	    
    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
            authentication.setAuthenticated(true);
        
        return Mono.just(authentication);
    }
        
}
