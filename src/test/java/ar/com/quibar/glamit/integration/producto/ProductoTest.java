package ar.com.quibar.glamit.integration.producto;

import static ar.com.quibar.glamit.categoria.CategoriaTestUtil.buildCategoria;
import static ar.com.quibar.glamit.producto.ProductoTestUtil.buildList;
import static ar.com.quibar.glamit.producto.ProductoTestUtil.buildProducto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriComponentsBuilder;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;
import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.producto.domain.port.ProductoRepository;


@SpringBootTest
@AutoConfigureWebTestClient(timeout = "20000")
@ActiveProfiles("test")
class ProductoTest {

	private static final int LIST_PRODUCTO_AMOUNT = 5;

	@Autowired
	private WebTestClient webClient;
	@Autowired
	private ProductoRepository productoReposity;
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	private Producto producto;
	private Categoria categoria;
	private List<Producto> productos;
	@BeforeEach
	void setup() {
		categoria = categoriaRepository.save(buildCategoria()).get();
		assertNotNull(categoria);
		
		producto = buildProducto(categoria);
		productos = buildList(LIST_PRODUCTO_AMOUNT, categoria);
		productos.add(producto);
		productos = productoReposity.saveAll(productos);
		assertNotNull(productos);		
	}
	
	@AfterEach
	void reset() {
		productoReposity.deleteAll();
		categoriaRepository.deleteAll();
	}
	
	@Test
	void createProductTest() {
		final var newProducto = buildProducto(categoria);
		final var saveProducto = webClient.post()
									.uri("/producto")
									.bodyValue(newProducto)
									.exchange()
									.expectStatus().isCreated()
									.expectBody(Producto.class)
									.returnResult().getResponseBody();
		assertNotNull(saveProducto);
	}
	
	@Test
	void createProductExistsTest(){
		final var newProducto = buildProducto(categoria);
		newProducto.setSku(producto.getSku());
		webClient.post()
			.uri("/producto")
			.bodyValue(newProducto)
			.exchange()
			.expectStatus().is4xxClientError();
	}
	
	@Test
	void findAllProductoTest() {
		var savedProductos = webClient.get()
								.uri(UriComponentsBuilder.fromPath("/producto")
										.queryParam("size", 6)
										.build().toUri())
								.exchange()
								.expectStatus().isOk()
								.expectBodyList(Producto.class)
								.returnResult().getResponseBody();
		System.out.println(savedProductos.size());
		assertNotNull(savedProductos);
		assertTrue(productos.containsAll(savedProductos));
		assertTrue(savedProductos.containsAll(productos));
		
	}

}
