package ar.com.quibar.glamit.shared.infrastructure.exceptions;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.server.ResponseStatusException;

public class InvalidResourceException extends ResponseStatusException {

	private static final long serialVersionUID = 3202068995983464897L;

	public InvalidResourceException(){
        super(BAD_REQUEST);
    }

    public InvalidResourceException(String message){
        super(BAD_REQUEST, message);
    }

}
