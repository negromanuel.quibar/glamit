package ar.com.quibar.glamit.producto.domain.port;

import java.util.List;
import java.util.Optional;

import ar.com.quibar.glamit.producto.domain.model.Producto;

public interface ProductoRepository {

	Optional<Producto> save(Producto producto);
	
	Optional<Producto> findBySku(String sku);
	
	List<Producto> findAll(Integer page, Integer size);
	
	void deleteAll();
	
	List<Producto> saveAll(List<Producto> productos);
	
	List<Producto> findAllBySku(Integer page, Integer size, String sku);
	
}
