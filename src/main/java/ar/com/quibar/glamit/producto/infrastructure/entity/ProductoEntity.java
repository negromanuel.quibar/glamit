package ar.com.quibar.glamit.producto.infrastructure.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.producto.domain.model.Producto;
import lombok.Builder;

@Document(value = "producto", collection = "producto")
@CompoundIndex(name = "sku", unique = true)
public class ProductoEntity extends Producto{

	@Builder
	public ProductoEntity(String id, String nombre, String urlImagen, String sku, Double precio, Categoria categoria) {
		super(id, nombre, urlImagen, sku, precio,categoria);
		this.categoriaId = categoria.getId();
	}
	
	public ProductoEntity(String id, String nombre, String urlImagen, String sku, Double precio, String categoriaId) {
		super(id, nombre, urlImagen, sku, precio, new Categoria());
		categoria.setId(categoriaId);
		this.categoriaId = categoriaId;
	}
	
	public ProductoEntity() {
	}

	@Id
	@Override
	public String getId() {
		return id;
	}
	
	protected String categoriaId;
	
	@Transient
	@Override
	public Categoria getCategoria() {
		return super.getCategoria();
	}
	
	@Override
	public void setCategoria(Categoria categoria) {
		super.setCategoria(categoria);
		this.categoriaId = categoria.getId();
	}
	
	public void setCategoriaId(String categoriaId) {
		this.categoriaId = categoriaId;
		var categoria = new Categoria();
		categoria.setId(categoriaId);
		super.setCategoria(categoria);
	}
	
	public String getCategoriaId() {
		return categoriaId;
	}

}
