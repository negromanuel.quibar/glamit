package ar.com.quibar.glamit.categoria.infrastructure.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;
import ar.com.quibar.glamit.categoria.infrastructure.entity.CategoriaEntity;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;

@Component
@RequiredArgsConstructor
public class MongoCategoriaRepository implements CategoriaRepository {

	private final SpringReactorMongoCategoriaRepository repository;
	private final MapperFacade mapperFacade;
	
	@Override
	public Optional<Categoria> save(Categoria categoria) {
		var entity = mapperFacade.map(categoria, CategoriaEntity.class);
		return Optional.of(repository.save(entity)
					.share()
					.block())
				.map(categoriaE -> mapperFacade.map(categoriaE, Categoria.class));
	}

	@Override
	public Optional<Categoria> findOneByNombre(String nombre) {
		return Optional.ofNullable(repository.findOneByNombre(nombre)
					.share()
					.block())
				.map(entity -> mapperFacade.map(entity, Categoria.class));
	}

	@Override
	public void deleteAll() {
		repository.deleteAll().share().block();
	}

	@Override
	public List<Categoria> findAll() {
		return Optional.of(repository.findAll()
					.collectList()
					.share()
					.block())
				.map(categorias -> mapperFacade.mapAsList(categorias, Categoria.class))
				.orElse(new ArrayList<>());
	}

	@Override
	public List<Categoria> saveAll(List<Categoria> categorias) {
		var entityList = mapperFacade.mapAsList(categorias, CategoriaEntity.class);
		return Optional.of(repository.saveAll(entityList)
					.collectList()
					.share()
					.block())
			.map(entity -> mapperFacade.mapAsList(entity, Categoria.class))
			.orElse(new ArrayList<>());
	}

}
