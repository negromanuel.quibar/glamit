package ar.com.quibar.glamit.producto.domain.usecase;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.producto.domain.port.ProductoRepository;

public class FindProducto {

	private final ProductoRepository productoRepository;
	
	public FindProducto(ProductoRepository productoRepository) {
		this.productoRepository = productoRepository;
	}
	
	public List<Producto> findProducto(Integer page, Integer size, String sku){
		if(StringUtils.isNotBlank(sku)) {
			return this.productoRepository.findAll(page, size);
		}
		return this.productoRepository.findAllBySku(page, size, sku);
	}
	
}
