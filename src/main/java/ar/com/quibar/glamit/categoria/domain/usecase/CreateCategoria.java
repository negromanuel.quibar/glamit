package ar.com.quibar.glamit.categoria.domain.usecase;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;

public class CreateCategoria {

	private final CategoriaRepository categoriaRepository;

	public CreateCategoria(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}
	
	
	public Categoria crearCategoria(Categoria categoria) {
		var optional = categoriaRepository.findOneByNombre(categoria.getNombre());
		if(optional.isPresent()) {
			return categoriaRepository.save(categoria).orElse(null);
		}
		return null;
	}
	
}
