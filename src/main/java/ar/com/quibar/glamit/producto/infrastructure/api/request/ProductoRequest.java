package ar.com.quibar.glamit.producto.infrastructure.api.request;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductoRequest {

	@NotBlank
	protected String nombre;
	protected String urlImagen;
	@NotBlank
	protected String sku;
	@NotNull
	protected Double precio;
	protected Categoria categoria;
}
