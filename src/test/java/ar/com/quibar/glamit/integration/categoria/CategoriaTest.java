package ar.com.quibar.glamit.integration.categoria;

import static ar.com.quibar.glamit.categoria.CategoriaTestUtil.buildCategoria;
import static ar.com.quibar.glamit.categoria.CategoriaTestUtil.buildListCategoria;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;

@SpringBootTest
@AutoConfigureWebTestClient(timeout = "20000")
@ActiveProfiles("test")
class CategoriaTest {

	private static final int LIST_CATEGORIA_AMOUNT = 5;
	@Autowired
	private WebTestClient webClient;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	private Categoria categoria;
	
	private List<Categoria> categorias;
	
	@BeforeEach
	void setup() {
		categoria = buildCategoria();
		categorias = buildListCategoria(LIST_CATEGORIA_AMOUNT);
		categorias.add(categoria);
		categorias = categoriaRepository.saveAll(categorias);
		assertNotNull(categorias);
	}
	
	@AfterEach
	void reset() {
		categoriaRepository.deleteAll();
	}
	
	@Test
	void findAllTest() {
		var savedCategorias = webClient.get()
				.uri("/categoria")
				.exchange()
				.expectStatus().isOk()
				.expectBodyList(Categoria.class)
				.returnResult().getResponseBody();
		
		assertNotNull(savedCategorias);
	}
}
