package ar.com.quibar.glamit.shared.infrastructure.configurations.mongo;


import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory;

import com.mongodb.ConnectionString;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MultipleMongoProperties.class)
public class MultipleMongoConfig {

	private final MultipleMongoProperties mongoProperties;

    
    @Bean(name = PrimaryMongoConfig.MONGO_TEMPLATE)
    @Primary
    public ReactiveMongoTemplate primaryMongoTemplate() throws Exception {
        return new ReactiveMongoTemplate(primaryFactory(mongoProperties.getPrimary()));
    }

    @Bean(name = SecondaryMongoConfig.MONGO_TEMPLATE)
    public ReactiveMongoTemplate secondaryMongoTemplate() throws Exception {
        return new ReactiveMongoTemplate(secondaryFactory(mongoProperties.getSecondary()));
    }

    @Bean
    @Primary
    public ReactiveMongoDatabaseFactory primaryFactory(final MongoProperties mongo) throws Exception {
        return new SimpleReactiveMongoDatabaseFactory(new ConnectionString(mongo.getUri()));
    }

    @Bean
    public ReactiveMongoDatabaseFactory secondaryFactory(final MongoProperties mongo) throws Exception {
    	return new SimpleReactiveMongoDatabaseFactory(new ConnectionString(mongo.getUri()));
    }
    
}
