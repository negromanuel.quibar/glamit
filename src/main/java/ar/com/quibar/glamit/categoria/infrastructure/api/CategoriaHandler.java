package ar.com.quibar.glamit.categoria.infrastructure.api;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import ar.com.quibar.glamit.categoria.domain.usecase.FindCategoria;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class CategoriaHandler {
	
	private final FindCategoria findCategoria;
	
	public Mono<ServerResponse> find(ServerRequest request){
		var categorias = findCategoria.findAll();
		return ok().bodyValue(categorias);
	}
}
