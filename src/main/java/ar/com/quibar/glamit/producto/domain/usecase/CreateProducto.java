package ar.com.quibar.glamit.producto.domain.usecase;

import ar.com.quibar.glamit.producto.domain.exceptions.ProductoConflictException;
import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.producto.domain.port.ProductoRepository;

public class CreateProducto {
	
	private final ProductoRepository productoRepository;

	public CreateProducto(ProductoRepository productoRepository) {
		this.productoRepository = productoRepository;
	}
	
	public Producto crearProducto(Producto producto) {		
		var optional = productoRepository.findBySku(producto.getSku());
		if(optional.isPresent()) {
			throw new ProductoConflictException("Producto codigo" +producto.getSku()+ " existente");
		}
		
		return productoRepository.save(producto).orElse(null);
		
	}
	

}
