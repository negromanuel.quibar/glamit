package ar.com.quibar.glamit.producto.domain.model;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.shared.domain.model.ObjetoConId;

public class Producto extends ObjetoConId{

	protected String nombre;
	protected String urlImagen;
	protected String sku;
	protected Double precio;
	protected Categoria categoria;
	public Producto() {
		super();
	}
	public Producto(String id, String nombre, String urlImagen, String sku, Double precio, Categoria categoria) {
		this.id = id;
		this.nombre = nombre;
		this.urlImagen = urlImagen;
		this.sku = sku;
		this.precio = precio;
		this.categoria = categoria;
	}
	

	public Producto(String nombre, String urlImagen, String sku, Double precio) {
		super();
		this.nombre = nombre;
		this.urlImagen = urlImagen;
		this.sku = sku;
		this.precio = precio;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", urlImagen=" + urlImagen + ", sku=" + sku + ", precio="
				+ precio + ", categoria=" + categoria + ", id=" + id + "]";
	}
	
	
	
}
