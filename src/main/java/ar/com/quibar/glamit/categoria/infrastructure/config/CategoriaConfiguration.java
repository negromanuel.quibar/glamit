package ar.com.quibar.glamit.categoria.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;
import ar.com.quibar.glamit.categoria.domain.usecase.FindCategoria;

@Configuration
public class CategoriaConfiguration {

	@Bean
	public FindCategoria findCategoria(CategoriaRepository categoriaRepository) {
		return new FindCategoria(categoriaRepository);
	}
}
