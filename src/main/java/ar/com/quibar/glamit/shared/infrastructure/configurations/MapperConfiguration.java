package ar.com.quibar.glamit.shared.infrastructure.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.NoArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
@NoArgsConstructor
public class MapperConfiguration {

    @Bean
    public MapperFactory mapperFactory() {
        return new DefaultMapperFactory.Builder()
                .mapNulls(false).build();
    }

    @Bean
    public MapperFacade mapperFacade(MapperFactory mapperFactory) {
      //  mapperFactory.getConverterFactory().registerConverter(new MoneyConverter());
        return mapperFactory.getMapperFacade();
    }
}
