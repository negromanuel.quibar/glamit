package ar.com.quibar.glamit.producto.infrastructure.exceptions;

import ar.com.quibar.glamit.shared.infrastructure.exceptions.InvalidResourceException;

public class ProductoHTTPInvalidException extends InvalidResourceException{

	private static final String MESSAGE = "Invalid producto";
	private static final long serialVersionUID = 4464186312033447252L;

	public ProductoHTTPInvalidException() {
		super(MESSAGE);
	}
	
	public ProductoHTTPInvalidException(String message) {
		super(message);
	}
	
	
}
