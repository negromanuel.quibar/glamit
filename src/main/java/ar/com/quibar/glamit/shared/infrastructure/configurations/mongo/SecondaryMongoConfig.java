package ar.com.quibar.glamit.shared.infrastructure.configurations.mongo;

import static ar.com.quibar.glamit.shared.infrastructure.configurations.mongo.SecondaryMongoConfig.MONGO_TEMPLATE;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackages = "ar.com.quibar.glamit.categoria.infrastructure",
	reactiveMongoTemplateRef = MONGO_TEMPLATE)
public class SecondaryMongoConfig {
	
	protected static final String MONGO_TEMPLATE = "secondaryMongoTemplate";

}
