package ar.com.quibar.glamit.categoria.domain.usecase;

import java.util.List;

import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.categoria.domain.port.CategoriaRepository;

public class FindCategoria {
	
	private final CategoriaRepository categoriaRepository;

	public FindCategoria(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}
	
	
	public List<Categoria> findAll(){
		return categoriaRepository.findAll();
	}
	

}
