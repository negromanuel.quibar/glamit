package ar.com.quibar.glamit.producto.infrastructure.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindProductoRequest {

	String page;
	String size;
	String sku;
	
	public int getPage() {
		return Integer.parseInt(page);
	}
	
	public int getSize() {
		return Integer.parseInt(size);
	}
	
}
