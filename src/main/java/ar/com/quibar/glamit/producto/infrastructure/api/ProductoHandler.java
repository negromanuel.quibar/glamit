package ar.com.quibar.glamit.producto.infrastructure.api;

import static org.springframework.web.reactive.function.server.ServerResponse.created;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import java.net.URI;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import ar.com.quibar.glamit.producto.domain.exceptions.ProductoConflictException;
import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.producto.domain.usecase.CreateProducto;
import ar.com.quibar.glamit.producto.domain.usecase.FindProducto;
import ar.com.quibar.glamit.producto.infrastructure.api.request.FindProductoRequest;
import ar.com.quibar.glamit.producto.infrastructure.api.request.ProductoRequest;
import ar.com.quibar.glamit.producto.infrastructure.exceptions.ProductoHTTPConflictException;
import ar.com.quibar.glamit.producto.infrastructure.validator.FindProductoValidator;
import ar.com.quibar.glamit.producto.infrastructure.validator.ProductoValidator;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ProductoHandler {

	private static final String PAGE = "page";
	private static final String SIZE = "size";
	private static final String SKU = "sku";
	private static final String DEFAULT_PAGE_STR = "0";
	private static final String DEFAULT_SIZE_STR = "5";
	private final CreateProducto crearProducto;
	private final FindProducto findProducto;
	private final ProductoValidator productoValidator;
	private final FindProductoValidator findValidator;
	private final MapperFacade mapperFacade;
	
	public Mono<ServerResponse> createProducto(ServerRequest request) {
		
		return request.bodyToMono(ProductoRequest.class)
				.doOnNext(productoValidator::validate)
				.map(productoRequest -> mapperFacade.map(productoRequest, Producto.class))
				.map(crearProducto::crearProducto)
				.onErrorResume(ProductoConflictException.class, e -> Mono.error(new ProductoHTTPConflictException(e.getMessage(), e)))
				.flatMap(producto -> created(URI.create(request.path() + "/" + producto.getId())).bodyValue(producto));
	}
	
	public Mono<ServerResponse> find(ServerRequest request){
		final var pageStr = request.queryParam(PAGE).orElse(DEFAULT_PAGE_STR);
		final var sizeStr = request.queryParam(SIZE).orElse(DEFAULT_SIZE_STR);
		final var sku = request.queryParam(SKU).orElse(StringUtils.EMPTY);
		
		final var findRequest = FindProductoRequest.builder()
									.page(pageStr)
									.size(sizeStr)
									.sku(sku)
									.build();
		findValidator.validate(findRequest);
		
		
		var productos = findProducto.findProducto(findRequest.getPage(),findRequest.getSize(), sku);
		return ok().bodyValue(productos);
	}
}
