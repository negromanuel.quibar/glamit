package ar.com.quibar.glamit.shared.infrastructure.exceptions;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.server.ResponseStatusException;


public class MissingParameterException extends ResponseStatusException {

    public MissingParameterException() {
        super(BAD_REQUEST);
    }

    public MissingParameterException(String message) {
        super(BAD_REQUEST, message);
    }
}
