package ar.com.quibar.glamit.producto.infrastructure.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import ar.com.quibar.glamit.categoria.infrastructure.repo.SpringReactorMongoCategoriaRepository;
import ar.com.quibar.glamit.producto.domain.model.Producto;
import ar.com.quibar.glamit.producto.domain.port.ProductoRepository;
import ar.com.quibar.glamit.producto.infrastructure.entity.ProductoEntity;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class MongoProductoRepository implements ProductoRepository{
	
	private final SpringReactorMongoProductoRepositoy productoRepository;
	private final SpringReactorMongoCategoriaRepository categoriaRepository;
	private final MapperFacade mapperFacade;
	
	@Override
	public Optional<Producto> save(Producto producto) {
		var entity = mapperFacade.map(producto, ProductoEntity.class);
		return Optional.of(productoRepository.save(entity).share().block())
				.map(productoSaved -> mapperFacade.map(productoSaved, Producto.class));
	}
	
	@Override
	public Optional<Producto> findBySku(String sku) {
		
		return Optional.ofNullable(productoRepository.findBySku(sku)
				.flatMap(producto -> categoriaRepository.findById(producto.getCategoriaId())
						.flatMap(categoria -> {
							producto.setCategoria(categoria);
							return Mono.just(producto);
						}))
				.share().block())
				.map(entity -> mapperFacade.map(entity, Producto.class));
	}

	@Override
	public void deleteAll() {
		productoRepository.deleteAll().share().block();
	}

	@Override
	public List<Producto> findAll(Integer page, Integer size) {
		return Optional.of(productoRepository.findAllPaged(PageRequest.of(page, size))
					.flatMap(producto -> categoriaRepository.findById(producto.getCategoriaId())
							.flatMap(categoria -> {
								producto.setCategoria(categoria);
								return Mono.just(producto);
							}))
			 		.collectList()
			 		.share()
			 		.block())
				.map(entity -> mapperFacade.mapAsList(entity, Producto.class))
				.orElse(new ArrayList<>());
	}
	
	@Override
	public List<Producto> saveAll(List<Producto> productos) {
		var entityList = mapperFacade.mapAsList(productos, ProductoEntity.class);
		return Optional.of(productoRepository.saveAll(entityList)
				.collectList()
				.share()
				.block())
		.map(entity -> mapperFacade.mapAsList(entity, Producto.class))
		.orElse(new ArrayList<>());
	}

	@Override
	public List<Producto> findAllBySku(Integer page, Integer size, String sku) {
		return Optional.of(productoRepository.findAllBySkuContainsIgnoreCase(sku, PageRequest.of(page, size))
				.flatMap(producto -> categoriaRepository.findById(producto.getCategoriaId())
						.flatMap(categoria -> {
							producto.setCategoria(categoria);
							return Mono.just(producto);
						}))
		 		.collectList()
		 		.share()
		 		.block())
			.map(entity -> mapperFacade.mapAsList(entity, Producto.class))
			.orElse(new ArrayList<>());
	}

}
