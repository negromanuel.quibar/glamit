package ar.com.quibar.glamit.producto.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ar.com.quibar.glamit.producto.domain.port.ProductoRepository;
import ar.com.quibar.glamit.producto.domain.usecase.CreateProducto;
import ar.com.quibar.glamit.producto.domain.usecase.FindProducto;

@Configuration
public class ProductoConfiguration {

	@Bean
	public CreateProducto crearProducto(ProductoRepository productoRepository) {
		return new CreateProducto(productoRepository);
	}
	
	@Bean
	public FindProducto findProduct(ProductoRepository productoRepository) {
		return new FindProducto(productoRepository);
	}
}
