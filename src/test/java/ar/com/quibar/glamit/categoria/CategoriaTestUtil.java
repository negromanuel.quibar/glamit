package ar.com.quibar.glamit.categoria;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import ar.com.quibar.glamit.TestUtils;
import ar.com.quibar.glamit.categoria.domain.model.Categoria;

public class CategoriaTestUtil extends TestUtils {

	public static Categoria buildCategoria() {
		Categoria categoria = new Categoria(RandomStringUtils.randomAlphabetic(ID_LENGTH),
				RandomStringUtils.randomNumeric(RANDOM_NUMBER_LENGTH));
		return categoria;
	}
	
	public static List<Categoria> buildListCategoria(int amount){
		final var listCategoria = new ArrayList<Categoria>();
		for (int i = 0; i < amount; i++) {
			listCategoria.add(buildCategoria());
		}
		return listCategoria;
	}
}
