package ar.com.quibar.glamit.categoria.infrastructure.config;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import ar.com.quibar.glamit.categoria.infrastructure.api.CategoriaHandler;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class CategoriaRouter {
	
	private final CategoriaHandler categoriaHandler;
	
	@Bean
	public RouterFunction<ServerResponse> routerCategoria() {
		return RouterFunctions
				.route(GET("/categoria").and(accept(APPLICATION_JSON)), categoriaHandler::find);
				
	}

}
