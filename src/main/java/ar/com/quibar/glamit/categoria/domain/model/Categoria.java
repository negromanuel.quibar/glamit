package ar.com.quibar.glamit.categoria.domain.model;

import ar.com.quibar.glamit.shared.domain.model.ObjetoConId;

public class Categoria extends ObjetoConId {

	private String nombre;
	private String codigo;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Categoria() {
	}
	public Categoria(String id, String nombre, String codigo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.codigo = codigo;
	}

	public Categoria(String nombre, String codigo) {
		super();
		this.nombre = nombre;
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "Categoria [nombre=" + nombre + ",codigo = " + codigo + ", id=" + id + "]";
	}
	
	
}
