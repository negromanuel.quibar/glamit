package ar.com.quibar.glamit.producto.infrastructure.config;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import ar.com.quibar.glamit.producto.infrastructure.api.ProductoHandler;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class ProductoRouter {
	private final ProductoHandler productoHandler;
	
	@Bean
	public RouterFunction<ServerResponse> routerProducto() {
		return RouterFunctions
				.route(POST("/producto").and(accept(APPLICATION_JSON)), productoHandler::createProducto)
				.andRoute(GET("/producto").and(accept(APPLICATION_JSON)), productoHandler::find);
				
	}
}
