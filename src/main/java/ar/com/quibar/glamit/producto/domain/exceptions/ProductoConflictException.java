package ar.com.quibar.glamit.producto.domain.exceptions;

public class ProductoConflictException extends RuntimeException{


	private static final long serialVersionUID = 2758909535153251614L;
	public static final String MESSAGE = "Property conflict";
	
	public ProductoConflictException() {
		super(MESSAGE);
	}
	
	public ProductoConflictException(String message) {
		super(message);
	}
	
	public ProductoConflictException(String message, Throwable cause) {
		super(message, cause);
	}
}
