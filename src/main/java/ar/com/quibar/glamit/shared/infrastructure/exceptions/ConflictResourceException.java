package ar.com.quibar.glamit.shared.infrastructure.exceptions;

import static org.springframework.http.HttpStatus.CONFLICT;

import org.springframework.web.server.ResponseStatusException;


public class ConflictResourceException extends ResponseStatusException {

	private static final long serialVersionUID = 9004050697539580137L;

	public ConflictResourceException(String message) {
        super(CONFLICT, message);
    }

    public ConflictResourceException(String message, Throwable cause) {
        super(CONFLICT, message, cause);
    }

    public ConflictResourceException(Throwable cause) {
        super(CONFLICT, null, cause);
    }
}
