package ar.com.quibar.glamit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlamitApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlamitApplication.class, args);
	}

}
