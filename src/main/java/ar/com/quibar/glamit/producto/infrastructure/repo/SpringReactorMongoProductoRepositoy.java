package ar.com.quibar.glamit.producto.infrastructure.repo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import ar.com.quibar.glamit.producto.infrastructure.entity.ProductoEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface SpringReactorMongoProductoRepositoy extends ReactiveMongoRepository<ProductoEntity, String>{
	
	Mono<ProductoEntity> findBySku(String sku);
	
	@Query("{ id: { $exists: true }}")
	Flux<ProductoEntity> findAllPaged(Pageable page);
	
	Flux<ProductoEntity> findAllBySkuContainsIgnoreCase(String sku, Pageable page);
}
