package ar.com.quibar.glamit.producto;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import ar.com.quibar.glamit.TestUtils;
import ar.com.quibar.glamit.categoria.domain.model.Categoria;
import ar.com.quibar.glamit.producto.domain.model.Producto;

public class ProductoTestUtil extends TestUtils{

	public static Producto buildProducto(Categoria categoria) {
		Producto producto = new Producto(
										RandomStringUtils.randomAlphabetic(RANDOM_STRING_LENGTH),
										RandomStringUtils.randomAlphabetic(RANDOM_STRING_LENGTH),
										RandomStringUtils.randomNumeric(RANDOM_NUMBER_LENGTH), RandomUtils.nextDouble());
		producto.setCategoria(categoria);
		return producto;
	}
	
	public static List<Producto> buildList(int amount, Categoria categoria){
		var list = new ArrayList<Producto>();
		for (int i = 0; i < amount; i++) {
			list.add( buildProducto(categoria));
		}
		return list;
	}
}
