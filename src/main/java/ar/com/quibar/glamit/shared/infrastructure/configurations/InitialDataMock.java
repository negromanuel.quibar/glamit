package ar.com.quibar.glamit.shared.infrastructure.configurations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import ar.com.quibar.glamit.categoria.infrastructure.entity.CategoriaEntity;
import ar.com.quibar.glamit.categoria.infrastructure.repo.SpringReactorMongoCategoriaRepository;
import ar.com.quibar.glamit.producto.infrastructure.entity.ProductoEntity;
import ar.com.quibar.glamit.producto.infrastructure.repo.SpringReactorMongoProductoRepositoy;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InitialDataMock {

	private static final int LIST_CATEGORIA_AMOUNT = 2;
	private static final int LIST_PRODUCTO_AMOUNT = 5;
	private final SpringReactorMongoCategoriaRepository categoriaRepo;
	private final SpringReactorMongoProductoRepositoy productoRepo;
	
	@PostConstruct
	public void crearDatos() {
		categoriaRepo.deleteAll().share().block();
		productoRepo.deleteAll().share().block();	
		List<CategoriaEntity> categorias = new ArrayList<>();
		for (int i = 0; i < LIST_CATEGORIA_AMOUNT; i++) {
			categorias.add(getCategoria(i));
		}
		categorias = categoriaRepo.saveAll(categorias).collectList().block();
		final List<ProductoEntity> productos = new ArrayList<>();
		var productoIndex = new AtomicInteger(1);
		categorias.forEach(categoria -> {
			for (int i = 0; i < LIST_PRODUCTO_AMOUNT; i++) {
				productos.add(getProducto(productoIndex.getAndIncrement(), categoria));
			}
		});
		productoRepo.saveAll(productos).collectList().block();
	}
	
	private CategoriaEntity getCategoria(int index) {
		return CategoriaEntity.builder()
				.nombre("Categoria_" + index)
				.codigo(Integer.toString(index))
				.build();
	}
	
	private ProductoEntity getProducto(int index, CategoriaEntity categoria) {
		return ProductoEntity.builder()
				.nombre("Producto_"+index)
				.sku("prod"+ Integer.toString(index))
				.precio(RandomUtils.nextDouble())
				.urlImagen("http://"+RandomStringUtils.randomAlphabetic(20))
				.categoria(categoria)
				.build();
				
	}
}
