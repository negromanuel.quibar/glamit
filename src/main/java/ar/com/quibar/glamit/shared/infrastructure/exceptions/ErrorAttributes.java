package ar.com.quibar.glamit.shared.infrastructure.exceptions;

import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@Component
public class ErrorAttributes extends DefaultErrorAttributes {


    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {

        final var error = getError(request);
        final var status = getStatusCode(error);

        Map<String, Object> map = super.getErrorAttributes(
                request, includeStackTrace);
        map.put("status", status);
        map.put("body", error.getMessage());
        map.put("error", status.getReasonPhrase());
        return map;
    }

    private HttpStatus getStatusCode(Throwable throwable) {
        if(throwable instanceof IllegalArgumentException) {
            return HttpStatus.BAD_REQUEST;
        }

        if(throwable instanceof ResponseStatusException) {
            return ((ResponseStatusException) throwable).getStatus();
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
