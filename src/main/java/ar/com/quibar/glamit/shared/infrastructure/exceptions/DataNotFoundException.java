package ar.com.quibar.glamit.shared.infrastructure.exceptions;

import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class DataNotFoundException extends ResponseStatusException {

	private static final long serialVersionUID = 1410683958540535600L;

	public DataNotFoundException() {
        super(NOT_FOUND);
    }

    public DataNotFoundException(String message) {
        super(NOT_FOUND,message);
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(NOT_FOUND, message, cause);
    }
}
