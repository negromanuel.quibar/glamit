package ar.com.quibar.glamit.categoria.infrastructure.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import ar.com.quibar.glamit.categoria.infrastructure.entity.CategoriaEntity;
import reactor.core.publisher.Mono;

@Repository
public interface SpringReactorMongoCategoriaRepository extends ReactiveMongoRepository<CategoriaEntity, String> {

	Mono<CategoriaEntity> findOneByNombre(String nombre);
}
