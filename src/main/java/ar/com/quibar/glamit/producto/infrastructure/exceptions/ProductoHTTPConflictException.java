package ar.com.quibar.glamit.producto.infrastructure.exceptions;

import ar.com.quibar.glamit.shared.infrastructure.exceptions.ConflictResourceException;

public class ProductoHTTPConflictException extends ConflictResourceException{

	private static final long serialVersionUID = -7562625426367236078L;
	private static final String MESSAGE = "Producto conflict";
	
	public ProductoHTTPConflictException() {
		super(MESSAGE);
	}
	
	public ProductoHTTPConflictException(String message) {
		super(message);
	}
	
	public ProductoHTTPConflictException(String message, Throwable cause) {
		super(message, cause);
	}
}
