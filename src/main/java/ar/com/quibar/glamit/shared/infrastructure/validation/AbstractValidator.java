package ar.com.quibar.glamit.shared.infrastructure.validation;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.quibar.glamit.shared.infrastructure.exceptions.InvalidResourceException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractValidator<T> implements Validator {

    private final Validator validator;

    public abstract Class<T> getValidationClass();

    @Override
    public boolean supports(Class<?> aClass) {
        return getValidationClass().isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        validator.validate(o, errors);
    }


    public void validate(Object o) {
        final Errors errors = new BeanPropertyBindingResult(getValidationClass(), getValidationClass().getName());
        validator.validate(o, errors);
        if (errors.hasErrors()) {
            throw new InvalidResourceException(errors.getAllErrors().toString());
        }
    }
}
